package test;

import static org.junit.Assert.*;

import org.junit.Test;

import sheridan.EmailValidator;

public class test {


	
	@Test
	public void testIsValidEmailtRegular() {
		 boolean result = EmailValidator.isValidEmail("aamir.nagra@gmail.com");
		assertTrue("valid Email",result);
	}
	
	@Test
	public void testIsValidEmailtException() {
		 boolean result = EmailValidator.isValidEmail("aamir.nagragmail.com");
		assertFalse("Invalid Email",result);
	}
	
	@Test
	public void testIsValidEmailtBoundaryIn() {
		 boolean result = EmailValidator.isValidEmail("aamir.nagra@gmail.co");
		assertTrue("valid mail",result);
	}
	
	@Test
	public void testIsValidEmailtBoundaryOut() {
		 boolean result = EmailValidator.isValidEmail("aamir.nagra@gmail..com");
		assertFalse("mail not valid ",result);
	}
	

}
